from constans import *


def config(port: int, nginx_domain: str, website_domain: str, email: str):
    """
    :param port: the port number the client's web connect to
    :param nginx_domain: the full domain of the nginx vm
    :param website_domain: the full domain of the client's web
    :param email: the client's email, for getting updates about certifications
    :return: a script file in 'plug N play' style, the client will run it on the nginx vm
    and will get our full service
    """
    with open("ACID-conf.py", 'w') as fp:
        fp.write(IMPORTS)
        fp.write(INSTALLATIONS)
        fp.write(ROOT_CA_FILE)
        fp.write(nginx_config(port, nginx_domain, website_domain))
        fp.write(certbot_config(nginx_domain, email))
        fp.write(EDIT_CRONTAB)


def certbot_config(nginx_domain: str, email: str):
    """
    :param nginx_domain: the full domain of the nginx vm
    :param email: the client's email, for getting updates about certifications
    :return: config the certbot - gets a certificate for our nginx server
    """
    certbot_command = "os.system('sudo REQUESTS_CA_BUNDLE=/etc/nginx/root_ca.crt certbot --nginx " \
                      "-m {0} -d {1} --server {2}')".format(email, nginx_domain, ACME_SERVER_DNS_DIRECTORY)
    return certbot_command


def nginx_config(port: int, nginx_domain: str, website_domain):
    """
    :param port: the port number the client's web connect to
    :param nginx_domain: the full domain of the nginx vm
    :param website_domain: the full domain of the client's web
    :return: config the nginx - sets the server to be secured reversed proxy for our client's web
    """
    nginx_conf_file = """
with open('/etc/nginx/nginx.conf', 'w') as fp:
    fp.write('''
user  www-data;
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  default_type application/octet-stream;
  log_format   main '$remote_addr - $remote_user [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  sendfile     on;
  tcp_nopush   on;

  server {
    listen       80;
    server_name  %s; 
    root         html;
    
    include /etc/nginx/default.d/*.conf;

    location / {
      proxy_pass https://%s:%d;
    }
  }
}
''') 
""" % (nginx_domain, website_domain, port)
    return nginx_conf_file


def edit_renewal_time(nginx_domain):
    """
    :param nginx_domain: the full domain of the nginx vm
    :return: edit the renewal certificates actions to be every 15 minutes, through crontab
    """
    new_renewal_time = """
with open('/etc/letsencrypt/renewal/{}', 'a') as fp:
    fp.write('''\n
    renew_before_expiry = 8 hours
    ''')
    """.format(nginx_domain)
    return new_renewal_time
