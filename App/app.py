from fastapi import FastAPI
from pydantic import BaseModel
from config import config
from starlette.responses import FileResponse

app = FastAPI()


class ReversedProxyParams(BaseModel):
    port: int
    nginx_domain: str
    website_domain: str
    email: str


@app.get("/")
async def create_item():
    return "welcome"


@app.post("/manifest")
def post_conf_file(params: ReversedProxyParams):
    config(params.port, params.nginx_domain, params.website_domain, params.email)
    file_name = "ACID-conf.py"
    return FileResponse("./" + file_name,
                        media_type='application/octet-stream', filename=file_name)
