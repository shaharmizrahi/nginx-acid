    
import os
import subprocess
    
try:
    subprocess.run(['sudo', 'yum', 'install', 'nginx'], check=True)
    subprocess.run(['sudo', 'yum', 'install', 'snapd'], check=True)
except subprocess.CalledProcessError:
    subprocess.run(['sudo', 'apt', 'install', 'nginx'], check=True)
    subprocess.run(['sudo', 'apt', 'install', 'snapd'], check=True)
finally:
    os.system('sudo snap install --classic certbot')

with open('/etc/nginx/root_ca.crt', 'w') as fp:
    fp.write('''
-----BEGIN CERTIFICATE-----
MIIBjzCCATWgAwIBAgIQJb5+5bGLpzaXQ+Gz9MubNDAKBggqhkjOPQQDAjAmMQ0w
CwYDVQQKEwRhY21lMRUwEwYDVQQDEwxhY21lIFJvb3QgQ0EwHhcNMjIwOTIyMTkw
OTQzWhcNMzIwOTE5MTkwOTQzWjAmMQ0wCwYDVQQKEwRhY21lMRUwEwYDVQQDEwxh
Y21lIFJvb3QgQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAT9VhmSuk70y621
dojthftHW4XfQnCQEKgBAa+LEirLb16PQFqQyn9eWDUn6mzcQPM8Yofy4jn3Q1hJ
Re30AGE+o0UwQzAOBgNVHQ8BAf8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBATAd
BgNVHQ4EFgQUgCXdRa5NJTYLRFoqlz998pquFUYwCgYIKoZIzj0EAwIDSAAwRQIh
APvaJyVrnOVZkF4nNnrwp6vLz7ZBE6qHPxNWYKg6vd1bAiBXaLzQ8yKALSdS3y6C
c9GT3pX3tXorh7IFmgRgq3rTVg==
-----END CERTIFICATE-----''')

with open('/etc/nginx/nginx.conf', 'w') as fp:
    fp.write('''
user  www-data;
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  default_type application/octet-stream;
  log_format   main '$remote_addr - $remote_user [$time_local]  $status '
    '"$request" $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';
  sendfile     on;
  tcp_nopush   on;

  server {
    listen       80;
    server_name  nginx-final.westeurope.cloudapp.azure.com; 
    root         html;
    
    include /etc/nginx/default.d/*.conf;

    location / {
      proxy_pass https://my-websire.com:80;
    }
  }
}
''') 
os.system('sudo REQUESTS_CA_BUNDLE=/etc/nginx/root_ca.crt certbot --nginx -m shahar@gmail.com -d nginx-final.westeurope.cloudapp.azure.com --server https://acme-mysql.westeurope.cloudapp.azure.com:8443/acme/acme/directory')
with open('/etc/crontab', 'w') as fp:
    fp.write('''
     */15 * * * * sudo REQUESTS_CA_BUNDLE=/etc/nginx/root_ca.crt certbot -q renew
     ''')
