NGINX_CONF_PATH = "/etc/nginx/nginx.conf"
ACME_SERVER_DNS_DIRECTORY = "https://acme-mysql.westeurope.cloudapp.azure.com:8443/acme/acme/directory"
ACME_SERVER_DNS = "https://nginx-try.westeurope.cloudapp.azure.com"
IMPORTS = """    
import os
import subprocess
"""
INSTALLATIONS = """    
try:
    subprocess.run(['sudo', 'yum', 'install', 'nginx'], check=True)
    subprocess.run(['sudo', 'yum', 'install', 'snapd'], check=True)
except subprocess.CalledProcessError:
    subprocess.run(['sudo', 'apt', 'install', 'nginx'], check=True)
    subprocess.run(['sudo', 'apt', 'install', 'snapd'], check=True)
finally:
    os.system('sudo snap install --classic certbot')
"""
EDIT_CRONTAB = """
with open('/etc/crontab', 'w') as fp:
    fp.write('''
     */15 * * * * sudo REQUESTS_CA_BUNDLE=/etc/nginx/root_ca.crt certbot -q renew
     ''')
"""
ROOT_CA_FILE = """
with open('/etc/nginx/root_ca.crt', 'w') as fp:
    fp.write('''
-----BEGIN CERTIFICATE-----
MIIBjzCCATWgAwIBAgIQJb5+5bGLpzaXQ+Gz9MubNDAKBggqhkjOPQQDAjAmMQ0w
CwYDVQQKEwRhY21lMRUwEwYDVQQDEwxhY21lIFJvb3QgQ0EwHhcNMjIwOTIyMTkw
OTQzWhcNMzIwOTE5MTkwOTQzWjAmMQ0wCwYDVQQKEwRhY21lMRUwEwYDVQQDEwxh
Y21lIFJvb3QgQ0EwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAT9VhmSuk70y621
dojthftHW4XfQnCQEKgBAa+LEirLb16PQFqQyn9eWDUn6mzcQPM8Yofy4jn3Q1hJ
Re30AGE+o0UwQzAOBgNVHQ8BAf8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBATAd
BgNVHQ4EFgQUgCXdRa5NJTYLRFoqlz998pquFUYwCgYIKoZIzj0EAwIDSAAwRQIh
APvaJyVrnOVZkF4nNnrwp6vLz7ZBE6qHPxNWYKg6vd1bAiBXaLzQ8yKALSdS3y6C
c9GT3pX3tXorh7IFmgRgq3rTVg==
-----END CERTIFICATE-----''')
"""